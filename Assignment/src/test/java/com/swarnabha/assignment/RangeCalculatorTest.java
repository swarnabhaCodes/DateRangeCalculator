package com.swarnabha.assignment;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.swarnabha.assignment.constants.TimePeriod;

import junit.framework.Assert;

public class RangeCalculatorTest {

  private RangeCalculator rangeCalculator;
  @Before
  public void setUp() throws Exception {
    // with the adapter pattern used, instead of initialzing the object here its done individual test case else we need to write spearate test classes for all
  }
  
  @Test
  public void testCalculateRangeDays() throws ParseException {
    rangeCalculator = new RangeCalculator(TimePeriod.DAY);
    List<String> expectedResult = new ArrayList<String>();
    expectedResult.add("04 May");
    expectedResult.add("05 May");
    List<String> result =  rangeCalculator.calculateRange("2013-05-04", "2013-05-05");
    
    Assert.assertEquals(expectedResult.size(), result.size());

    for (int i = 0; i < expectedResult.size(); i++) {
      Assert.assertEquals(expectedResult.get(i), result.get(i));
    }
  }

  @Test
  public void testCalculateRangeQuarters() throws ParseException {
    rangeCalculator = new RangeCalculator(TimePeriod.QUARTER);
    List<String> expectedResult = new ArrayList<String>();
    expectedResult.add("Jun13");
    expectedResult.add("Sep13");
    expectedResult.add("Dec13");
    expectedResult.add("Mar14");
    expectedResult.add("Jun14");
    expectedResult.add("Sep14");
    List<String> result =  rangeCalculator.calculateRange("2013-05-01", "2014-09-02");
    
    Assert.assertEquals(expectedResult.size(), result.size());

    for (int i = 0; i < expectedResult.size(); i++) {
      Assert.assertEquals(expectedResult.get(i), result.get(i));
    }
  }
  
  @Test
  public void testCalculateRangeMonth() throws ParseException {
    rangeCalculator = new RangeCalculator(TimePeriod.MONTH);

    List<String> expectedResult = new ArrayList<String>();
    expectedResult.add("May13");
    expectedResult.add("Jun13");
    expectedResult.add("Jul13");
    expectedResult.add("Aug13");
    expectedResult.add("Sep13");
    expectedResult.add("Oct13");
    expectedResult.add("Nov13");
    expectedResult.add("Dec13");
    expectedResult.add("Jan14");
    expectedResult.add("Feb14");
    expectedResult.add("Mar14");
    expectedResult.add("Apr14");
    expectedResult.add("May14");
    expectedResult.add("Jun14");
    expectedResult.add("Jul14");
    expectedResult.add("Aug14");

    expectedResult.add("Sep14");
    expectedResult.add("Oct14");
    List<String> result =  rangeCalculator.calculateRange("2013-05-01", "2014-09-02");
 
    Assert.assertEquals(expectedResult.size(), result.size());

    for (int i = 0; i < expectedResult.size(); i++) {
      Assert.assertEquals(expectedResult.get(i), result.get(i));
    }
  }
  
  @Test
  public void testCalculateRangeMonthLeapYr() throws ParseException {
    rangeCalculator = new RangeCalculator(TimePeriod.MONTH);

    List<String> expectedResult = new ArrayList<String>();
    expectedResult.add("May15");
    expectedResult.add("Jun15");
    expectedResult.add("Jul15");
    expectedResult.add("Aug15");
    expectedResult.add("Sep15");
    expectedResult.add("Oct15");
    expectedResult.add("Nov15");
    expectedResult.add("Dec15");
    expectedResult.add("Jan16");
    expectedResult.add("Feb16");
    expectedResult.add("Mar16");
    expectedResult.add("Apr16");
    expectedResult.add("May16");
    expectedResult.add("Jun16");
    expectedResult.add("Jul16");
    expectedResult.add("Aug16");
    expectedResult.add("Sep16");
    expectedResult.add("Oct16");
    List<String> result =  rangeCalculator.calculateRange("2015-05-01", "2016-09-02");
 
    Assert.assertEquals(expectedResult.size(), result.size());

    for (int i = 0; i < expectedResult.size(); i++) {
      Assert.assertEquals(expectedResult.get(i), result.get(i));
    }
  }

  @Test
  public void testCalculateRangeYear() throws ParseException {
    rangeCalculator = new RangeCalculator(TimePeriod.YEAR);

    List<String> expectedResult = new ArrayList<String>();
    expectedResult.add("2013");
    expectedResult.add("2014");

    List<String> result =  rangeCalculator.calculateRange("2013-05-01", "2014-09-02");
    
    Assert.assertEquals(expectedResult.size(), result.size());
    
    for (int i = 0; i < expectedResult.size(); i++) {
      Assert.assertEquals(expectedResult.get(i), result.get(i));
    }
  }

}
