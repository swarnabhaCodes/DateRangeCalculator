package com.swarnabha.assignment;

import java.text.ParseException;
import java.util.List;

import com.swarnabha.assignment.constants.TimePeriod;
import com.swarnabha.assignment.range.service.DayRangeService;
import com.swarnabha.assignment.range.service.MonthRangeService;
import com.swarnabha.assignment.range.service.QuartersRangeService;
import com.swarnabha.assignment.range.service.RangeService;
import com.swarnabha.assignment.range.service.YearRangeService;

/**
 * 
 * @author swarnabha_lahiri
 *
 */
public class RangeCalculator {

  private RangeService rangeService;
  
  public RangeCalculator(TimePeriod period){
    switch(period){
      case DAY: rangeService = new DayRangeService();
      break;
      case QUARTER: rangeService = new QuartersRangeService();
      break;
      case MONTH: rangeService = new MonthRangeService();
        break;
      case YEAR: rangeService = new YearRangeService();
        break;
    }
  }
  
  public List<String> calculateRange(String startDtStr,String endDateStr) throws ParseException{
    return rangeService.getRange(startDtStr, endDateStr);
  }
}
