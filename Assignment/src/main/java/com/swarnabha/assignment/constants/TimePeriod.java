package com.swarnabha.assignment.constants;

/**
 * 
 * @author swarnabha_lahiri
 *
 */
public enum TimePeriod {
  DAY,MONTH,QUARTER,YEAR;
}
