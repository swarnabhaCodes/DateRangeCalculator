package com.swarnabha.assignment.range.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;


/**
 * 
 * @author swarnabha_lahiri
 *
 */
public class DayRangeService implements RangeService {
  
  private static final SimpleDateFormat DATE_FORMAT_IN = new SimpleDateFormat("yyyy-MM-dd");
  private static final SimpleDateFormat DATE_FORMAT_OUT_DAY = new SimpleDateFormat("dd MMM");


  /**
   * 
   * @param startDateStr
   * @param endDateStr
   * @return
   * @throws ParseException
   */
  @Override
  public List<String> getRange(String startDateStr, String endDateStr) throws ParseException {

    java.util.Date startDate = DATE_FORMAT_IN.parse(startDateStr);
    // System.out.println(startQtrEndDate);
    java.util.Date endDate = DATE_FORMAT_IN.parse(endDateStr);
    // System.out.println(endQtrEndDate);
    List<String> elapsedDates = new ArrayList<String>();
    elapsedDates.add(DATE_FORMAT_OUT_DAY.format(startDate));
    while (endDate.after(startDate)) {
      DateTime dateTime = new DateTime(startDate.getTime());
      dateTime = dateTime.plusDays(1);
      startDate = new Date(dateTime.getMillis());
      elapsedDates.add(DATE_FORMAT_OUT_DAY.format(startDate));
    }
    return elapsedDates;
  }

}
