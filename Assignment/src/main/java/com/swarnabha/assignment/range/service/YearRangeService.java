package com.swarnabha.assignment.range.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import org.joda.time.DateTime;

import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.TreeRangeMap;

/**
 * 
 * @author swarnabha_lahiri
 *
 */
public class YearRangeService implements RangeService {
  private static final SimpleDateFormat DATE_FORMAT_IN = new SimpleDateFormat("yyyy-MM-dd");
  private static final SimpleDateFormat DATE_FORMAT_OUT_YEAR = new SimpleDateFormat("yyyy");

  private RangeMap<java.util.Date, Integer> getYearForDate(java.util.Date date) {
    RangeMap<java.util.Date, Integer> year = TreeRangeMap.create();
    java.util.Date dateToCompare;
    // checkNotNull(date);

    dateToCompare = date;

    // create a JODA current time instance
    DateTime currentDate = new DateTime(dateToCompare);
    // set quarters
    DateTime yBeginDate = new DateTime(currentDate.getYear(), 1, 1, 0, 0, 0, 0);
    DateTime yEndDate = new DateTime(currentDate.getYear(), 12, 31, 0, 0, 0, 0);

    year.put(Range.closed(yBeginDate.toDate(), yEndDate.toDate()), 1);

    return year;
  }

  private java.util.Date getYearEndDate(java.util.Date inputDate) {

    RangeMap<java.util.Date, Integer> year = getYearForDate(inputDate);
    Entry<Range<java.util.Date>, Integer> yearAsDate = year.getEntry(inputDate);

    Range<java.util.Date> range = yearAsDate.getKey();

    return range.upperEndpoint();
  }

  /**
   * 
   * @param startDateStr
   * @param endDateStr
   * @return
   * @throws ParseException
   */
  @Override
  public List<String> getRange(String startDateStr, String endDateStr) throws ParseException {

    java.util.Date startDate = DATE_FORMAT_IN.parse(startDateStr);
    java.util.Date startYearEndDate = getYearEndDate(startDate);
    // System.out.println(startQtrEndDate);
    java.util.Date endDate = DATE_FORMAT_IN.parse(endDateStr);
    java.util.Date endYearEndDate = getYearEndDate(endDate);
    // System.out.println(endQtrEndDate);
    List<String> elapsedYearEndDates = new ArrayList<String>();
    elapsedYearEndDates.add(DATE_FORMAT_OUT_YEAR.format(startYearEndDate));
    while (endYearEndDate.after(startYearEndDate)) {
      DateTime dateTime = new DateTime(startYearEndDate.getTime());
      dateTime = dateTime.plusYears(1);
      startYearEndDate = new Date(dateTime.getMillis());
      elapsedYearEndDates.add(DATE_FORMAT_OUT_YEAR.format(startYearEndDate));
    }
    return elapsedYearEndDates;
  }
}
