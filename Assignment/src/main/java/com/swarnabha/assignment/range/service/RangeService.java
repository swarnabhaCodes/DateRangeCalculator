package com.swarnabha.assignment.range.service;

import java.text.ParseException;
import java.util.List;

/**
 * 
 * @author swarnabha_lahiri
 *
 */
public interface RangeService {
  
  /**
   * 
   * @param startDtStr
   * @param endDateStr
   * @return
   * @throws ParseException
   */
  List<String> getRange(String startDtStr, String endDateStr) throws ParseException;

}
