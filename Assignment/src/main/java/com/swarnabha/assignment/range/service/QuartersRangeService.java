package com.swarnabha.assignment.range.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import org.joda.time.DateTime;

import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.TreeRangeMap;

/**
 * 
 * @author swarnabha_lahiri
 *
 */
public class QuartersRangeService implements RangeService {
  
  private static final SimpleDateFormat DATE_FORMAT_IN = new SimpleDateFormat("yyyy-MM-dd");
  private static final SimpleDateFormat DATE_FORMAT_OUT = new SimpleDateFormat("MMMyy");

  private RangeMap<java.util.Date, Integer> getQuartersForDate(java.util.Date date) {
    RangeMap<java.util.Date, Integer> quarters = TreeRangeMap.create();
    java.util.Date dateToCompare;
    // checkNotNull(date);

    dateToCompare = date;

    // create a JODA current time instance
    DateTime currentDate = new DateTime(dateToCompare);
    // set quarters
    DateTime q1BeginDate = new DateTime(currentDate.getYear(), 1, 1, 0, 0, 0, 0);
    DateTime q1EndDate = new DateTime(currentDate.getYear(), 3, 31, 0, 0, 0, 0);

    DateTime q2BeginDate = new DateTime(currentDate.getYear(), 4, 1, 0, 0, 0, 0);
    DateTime q2EndDate = new DateTime(currentDate.getYear(), 6, 30, 0, 0, 0, 0);

    DateTime q3BeginDate = new DateTime(currentDate.getYear(), 7, 1, 0, 0, 0, 0);
    DateTime q3EndDate = new DateTime(currentDate.getYear(), 9, 30, 0, 0, 0, 0);

    DateTime q4BeginDate = new DateTime(currentDate.getYear(), 10, 1, 0, 0, 0, 0);
    DateTime q4EndDate = new DateTime(currentDate.getYear(), 12, 31, 0, 0, 0, 0);

    // add to quarters
    quarters.put(Range.closed(q1BeginDate.toDate(), q1EndDate.toDate()), 1);
    quarters.put(Range.closed(q2BeginDate.toDate(), q2EndDate.toDate()), 2);
    quarters.put(Range.closed(q3BeginDate.toDate(), q3EndDate.toDate()), 3);
    quarters.put(Range.closed(q4BeginDate.toDate(), q4EndDate.toDate()), 4);

    return quarters;
  }

  private java.util.Date getQuarterEndDate(java.util.Date inputDate) {

    RangeMap<java.util.Date, Integer> quarters = getQuartersForDate(inputDate);
    Entry<Range<java.util.Date>, Integer> quarterAsDate = quarters.getEntry(inputDate);

    Range<java.util.Date> range = quarterAsDate.getKey();

    return range.upperEndpoint();
  }

  /**
   * 
   * @param startDateStr
   * @param endDateStr
   * @return
   * @throws ParseException
   */
  @Override
  public List<String> getRange (String startDateStr,String endDateStr) throws ParseException {
    
    java.util.Date startDate = DATE_FORMAT_IN.parse(startDateStr);
    java.util.Date startQtrEndDate = getQuarterEndDate(startDate);
    //System.out.println(startQtrEndDate);
    java.util.Date endDate = DATE_FORMAT_IN.parse(endDateStr);
    java.util.Date endQtrEndDate = getQuarterEndDate(endDate);
    //System.out.println(endQtrEndDate);
    List<String> elapsedQtrEndDates = new ArrayList<String>();
    elapsedQtrEndDates.add(DATE_FORMAT_OUT.format(startQtrEndDate));
    while(endQtrEndDate.after(startQtrEndDate)){
      DateTime dateTime = new DateTime(startQtrEndDate.getTime());
      dateTime =dateTime.plusMonths(3);
      startQtrEndDate = new Date(dateTime.getMillis());
      elapsedQtrEndDates.add(DATE_FORMAT_OUT.format(startQtrEndDate));
    }
    return elapsedQtrEndDates;
  }

}
