package com.swarnabha.assignment.range.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import org.joda.time.DateTime;

import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.TreeRangeMap;


/**
 * 
 * @author swarnabha_lahiri
 *
 */
public class MonthRangeService implements RangeService {
  private static final SimpleDateFormat DATE_FORMAT_IN = new SimpleDateFormat("yyyy-MM-dd");
  private static final SimpleDateFormat DATE_FORMAT_OUT = new SimpleDateFormat("MMMyy");


  private RangeMap<java.util.Date, Integer> getMonthsForDate(java.util.Date date) {
    RangeMap<java.util.Date, Integer> months = TreeRangeMap.create();
    java.util.Date dateToCompare;
    // checkNotNull(date);

    dateToCompare = date;

    // create a JODA current time instance
    DateTime currentDate = new DateTime(dateToCompare);
    // set quarters
    DateTime m1BeginDate = new DateTime(currentDate.getYear(), 1, 1, 0, 0, 0, 0);
    DateTime m1EndDate = new DateTime(currentDate.getYear(), 1, 31, 0, 0, 0, 0);

    DateTime m2BeginDate = new DateTime(currentDate.getYear(), 2, 1, 0, 0, 0, 0);
    DateTime m2EndDate = new DateTime(currentDate.getYear(), 2, isLeapYear(currentDate.getYear())?29:28, 0, 0, 0, 0);

    DateTime m3BeginDate = new DateTime(currentDate.getYear(), 3, 1, 0, 0, 0, 0);
    DateTime m3EndDate = new DateTime(currentDate.getYear(), 3, 31, 0, 0, 0, 0);

    DateTime m4BeginDate = new DateTime(currentDate.getYear(), 4, 1, 0, 0, 0, 0);
    DateTime m4EndDate = new DateTime(currentDate.getYear(), 4, 30, 0, 0, 0, 0);
    

    DateTime m5BeginDate = new DateTime(currentDate.getYear(), 5, 1, 0, 0, 0, 0);
    DateTime m5EndDate = new DateTime(currentDate.getYear(), 5, 31, 0, 0, 0, 0);

    DateTime m6BeginDate = new DateTime(currentDate.getYear(), 6, 1, 0, 0, 0, 0);
    DateTime m6EndDate = new DateTime(currentDate.getYear(), 6, 30, 0, 0, 0, 0);
    

    DateTime m7BeginDate = new DateTime(currentDate.getYear(), 7, 1, 0, 0, 0, 0);
    DateTime m7EndDate = new DateTime(currentDate.getYear(), 7, 31, 0, 0, 0, 0);

    DateTime m8BeginDate = new DateTime(currentDate.getYear(), 8, 1, 0, 0, 0, 0);
    DateTime m8EndDate = new DateTime(currentDate.getYear(), 8, 31, 0, 0, 0, 0);
    
    DateTime m9BeginDate = new DateTime(currentDate.getYear(), 9, 1, 0, 0, 0, 0);
    DateTime m9EndDate = new DateTime(currentDate.getYear(), 9, 30, 0, 0, 0, 0);

    DateTime m10BeginDate = new DateTime(currentDate.getYear(), 10, 1, 0, 0, 0, 0);
    DateTime m10EndDate = new DateTime(currentDate.getYear(), 10, 31, 0, 0, 0, 0);
    

    DateTime m11BeginDate = new DateTime(currentDate.getYear(), 11, 1, 0, 0, 0, 0);
    DateTime m11EndDate = new DateTime(currentDate.getYear(), 11, 30, 0, 0, 0, 0);

    DateTime m12BeginDate = new DateTime(currentDate.getYear(), 12, 1, 0, 0, 0, 0);
    DateTime m12EndDate = new DateTime(currentDate.getYear(), 12, 31, 0, 0, 0, 0);

    // add to quarters
    months.put(Range.closed(m1BeginDate.toDate(), m1EndDate.toDate()), 1);
    months.put(Range.closed(m2BeginDate.toDate(), m2EndDate.toDate()), 2);
    months.put(Range.closed(m3BeginDate.toDate(), m3EndDate.toDate()), 3);
    months.put(Range.closed(m4BeginDate.toDate(), m4EndDate.toDate()), 4);
    months.put(Range.closed(m5BeginDate.toDate(), m5EndDate.toDate()), 5);
    months.put(Range.closed(m6BeginDate.toDate(), m6EndDate.toDate()), 6);
    months.put(Range.closed(m7BeginDate.toDate(), m7EndDate.toDate()), 7);
    months.put(Range.closed(m8BeginDate.toDate(), m8EndDate.toDate()), 8);
    months.put(Range.closed(m9BeginDate.toDate(), m9EndDate.toDate()), 9);
    months.put(Range.closed(m10BeginDate.toDate(), m10EndDate.toDate()), 10);
    months.put(Range.closed(m11BeginDate.toDate(), m11EndDate.toDate()), 11);
    months.put(Range.closed(m12BeginDate.toDate(), m12EndDate.toDate()), 12);
    

    return months;
  }
  
  private boolean isLeapYear(int year) {
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.YEAR, year);
    int days =cal.getActualMaximum(Calendar.DAY_OF_YEAR);
    return days > 365;
  }
  
  public java.util.Date getMonthEndDate(java.util.Date inputDate) {

    RangeMap<java.util.Date, Integer> months = getMonthsForDate(inputDate);
    Entry<Range<java.util.Date>, Integer> monthAsDate = months.getEntry(inputDate);

    Range<java.util.Date> range = monthAsDate.getKey();

    return range.upperEndpoint();
  }
  
  /**
   * 
   * @param startDateStr
   * @param endDateStr
   * @return
   * @throws ParseException
   */
  @Override
  public List<String> getRange (String startDateStr,String endDateStr) throws ParseException {
    
    java.util.Date startDate = DATE_FORMAT_IN.parse(startDateStr);
    java.util.Date startMonthEndDate = getMonthEndDate(startDate);
    //System.out.println(startQtrEndDate);
    java.util.Date endDate = DATE_FORMAT_IN.parse(endDateStr);
    java.util.Date endMonthEndDate = getMonthEndDate(endDate);
    //System.out.println(endQtrEndDate);
    List<String> elapsedMonthEndDates = new ArrayList<String>();
    elapsedMonthEndDates.add(DATE_FORMAT_OUT.format(startMonthEndDate));
    while(endMonthEndDate.after(startMonthEndDate)){
      DateTime dateTime = new DateTime(startMonthEndDate.getTime());
      dateTime =dateTime.plusMonths(1);
      startMonthEndDate = new Date(dateTime.getMillis());
      elapsedMonthEndDates.add(DATE_FORMAT_OUT.format(startMonthEndDate));
    }
    return elapsedMonthEndDates;
  }

}
